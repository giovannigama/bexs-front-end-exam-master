# Exame - Bexs Front-end

## Desenvolvedor

Giovanni Parize Gama

## Solução

O desafio proposto visou a criação da segunda etapa de uma tela de pagamento utilizando cartão de crédito, seguindo o layout oferecido.

O projeto foi desenvolvido utilizando HTML5, SCSS, Bootstrap e Angular 9, responsivo.

O layout seguiu as definições da tela *Home*, bem como as versões Desktop e Mobile apresentadas.

Ao digitar os dados do cartão no formulário, os dados são apresentados no cartão à esquerda, onde o usuário pode visualizar as informações. As bandeiras dos principais cartões disponíveis no Brasil (American Express, Dinners Club, Elo, Hiper, Hipercard, Mastercard e Visa) são apresentadas assim que o cartão correspondente é identificado.

## Como rodar o projeto

### Rodando a build

1. Para rodar a build da pasta `/dist`, tenha instalado o `http-server` (`npm install http-server -g`);

2. Acesse a pasta do projeto e execute o comando `http-server dist/bexs-angular`.

### Rodando o projeto em desenvolvimento

1. Clone o projeto em seu computador;

2. Acesse a pasta do projeto e execute o comando `npm install`;

3. Execute o comando `ng serve`;

4. Após a compilação, acesse o endereço `http://localhost:4200/` através do navegador;

5. Para gerar uma build, digite o comando `ng build`.

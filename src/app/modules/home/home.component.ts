import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';

import * as creditCardType from 'credit-card-type';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  paymentForm: FormGroup;

  cardBrand: any;
  numCard = '**** **** **** ****';
  nameCard = 'Nome do titular';
  dateCard = '00/00';
  cvvCard = '* * *';

  formError = {
    'num': false,
    'name': false,
    'expire': false,
    'cvv': false,
    'installments': false
  };

  showModal: boolean = false;

  messageTitle: string;
  messageText: string;

  constructor(
    private formBuilder: FormBuilder,
    public http: Http,
  ) {
    this.createForm();
  }

  private createForm() {
    this.paymentForm = this.formBuilder.group({
      cardNum: ['', Validators.required],
      cardName: ['', Validators.required],
      cardExpire: ['', Validators.required],
      cardCVV: ['', Validators.required],
      installments: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  openMenu() {
    if( document.getElementsByClassName('hamburger-menu')[0].classList.contains('open') ) {
      document.getElementsByClassName('hamburger-menu')[0].classList.remove('open');
      document.getElementsByClassName('menu')[0].classList.remove('open');
    } else {
      document.getElementsByClassName('hamburger-menu')[0].classList.add('open');
      document.getElementsByClassName('menu')[0].classList.add('open');
    }
  }

  closeModal() {
    this.showModal = false;
  }

  flipCard() {
    if( document.getElementsByClassName('credit-card-box')[0].classList.contains('hover') ) {
      document.getElementsByClassName('credit-card-box')[0].classList.remove('hover');
    } else {
      document.getElementsByClassName('credit-card-box')[0].classList.add('hover');
    }
  }

  openFrontCard() {
    document.getElementsByClassName('credit-card-box')[0].classList.remove('hover');
  }

  openCVV() {
    document.getElementsByClassName('credit-card-box')[0].classList.add('hover');
  }

  closeCVV() {
    document.getElementsByClassName('credit-card-box')[0].classList.remove('hover');
  }

  checkNumCard() {
    if ( this.paymentForm.controls['cardNum'].value ) {
      this.numCard = this.paymentForm.controls['cardNum'].value;

      if( this.paymentForm.controls['cardNum'].value.length >= 3 ){
        // console.log();
        if( this.paymentForm.controls['cardNum'].value.substr(0,3) == "636" ) {
          this.cardBrand = "elo";
        } else if( this.paymentForm.controls['cardNum'].value.substr(0,3) == "637" ) {
          this.cardBrand = "hiper";
        } else if( this.paymentForm.controls['cardNum'].value.substr(0,3) == "606" ) {
          this.cardBrand = "hipercard";
        } else if( creditCardType(this.paymentForm.controls['cardNum'].value) ) {
          this.cardBrand = creditCardType(this.paymentForm.controls['cardNum'].value)[0]['type'];
        } else {
          this.cardBrand = null;
        }
      } else {
        this.cardBrand = null;
      }
    } else {
      this.numCard = "**** **** **** ****";
      this.cardBrand = null;
    }
  }

  checkNameCard() {
    if ( this.paymentForm.controls['cardName'].value ) {
      this.nameCard = this.paymentForm.controls['cardName'].value;
    } else {
      this.nameCard = 'Nome do titular';
    }
  }

  checkDateCard() {
    if ( this.paymentForm.controls['cardExpire'].value ) {
      this.dateCard = this.paymentForm.controls['cardExpire'].value;
    } else {
      this.dateCard = '00/00';
    }
  }

  checkCvvCard() {
    if ( this.paymentForm.controls['cardCVV'].value ) {
      this.cvvCard = this.paymentForm.controls['cardCVV'].value;
    } else {
      this.cvvCard = '* * *';
    }
  }

  validaForm(form: any) {
    this.formError = {
      'num': false,
      'name': false,
      'expire': false,
      'cvv': false,
      'installments': false
    };

    if ( !this.paymentForm.controls['cardNum'].value || this.paymentForm.controls['cardNum'].value.length != 18 && this.cardBrand == "american-express" || this.paymentForm.controls['cardNum'].value.length < 19 && this.cardBrand != "american-express" ) {
      this.formError['num'] = true;
    }

    if ( !this.paymentForm.controls['cardName'].value || this.paymentForm.controls['cardName'].value.indexOf(' ') < 0 ) {
      this.formError['name'] = true;
    } else {
      var splitValue = this.paymentForm.controls['cardName'].value.split(' ');

      if( splitValue[1] == "" ) {
        this.formError['name'] = true;
      }
    }

    if ( !this.paymentForm.controls['cardExpire'].value || this.paymentForm.controls['cardExpire'].value.length < 5 ) {
      this.formError['expire'] = true;
    } else {
      var expireDate = this.paymentForm.controls['cardExpire'].value.split('/');

      console.log(parseInt(expireDate[0]), parseInt(expireDate[1]) + 2000);

      if ( parseInt(expireDate[1]) + 2000 < new Date().getFullYear() ) {
        this.formError['expire'] = true;
      } else if ( parseInt(expireDate[1]) + 2000 == new Date().getFullYear() && parseInt(expireDate[0]) - 1 < new Date().getMonth() ) {
        this.formError['expire'] = true;
      } else if ( parseInt(expireDate[0]) == 0 || parseInt(expireDate[0]) > 12 ) {
        this.formError['expire'] = true;
      }
    }

    if ( !this.paymentForm.controls['cardCVV'].value ) {
      this.formError['cvv'] = true;
    } else if ( this.paymentForm.controls['cardCVV'].value.length != 3 && this.cardBrand != 'american-express' ) {
      this.formError['cvv'] = true;
    } else if( this.cardBrand == 'american-express' && this.paymentForm.controls['cardCVV'].value.length != 4 ) {
      this.formError['cvv'] = true;
    }

    if ( !this.paymentForm.controls['installments'].value ) {
      this.formError['installments'] = true;
    }

    if( Object.values(this.formError).indexOf(true) == -1 ) {
      var url = `/pagar`;
      var headers = new Headers({ 'Content-Type': 'application/json' });
      var options = new RequestOptions({ headers: headers});
      this.http.post(url, form, options)
      .pipe(map(res => JSON.parse(res['_body'])))
      .subscribe(res => {
        console.log(res);
        this.showModal = true;

        this.messageTitle = 'Pagamento enviado';
        this.messageText = 'Seu pagamento foi enviado e está em análise.';

        this.paymentForm.reset();
      }, error => {
        console.log(error);
        this.showModal = true;

        this.messageTitle = 'Ocorreu um erro';
        this.messageText = 'Ocorreu um erro ao processar seu pagamento.';

        this.paymentForm.reset();
      });
    }
  }
}
